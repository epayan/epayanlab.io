---
layout: default
---

<h1>Posts</h1>

<ul style="list-style-type:none;">
  {% for post in site.posts %}
    <li>
      <div class="image-circular singleLine">
        <a href="{{ post.url }}"><img src="{{ post.title_image }}"></a>
      </div> 
      <div class="singleLine"> 
        <a href="{{ post.url }}">{{ post.title }}</a> 
      </div> 
      <p> {{ post.excerpt }}</p>
      <br />
      <hr class="solid">
      <br />
      <br />
    </li>
   
  {% endfor %}     
</ul>